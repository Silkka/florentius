import re

def parse_character(s):
    name = re.search(r"[a-zA-Z'. ]+",s)
    if name:
        name = name.group().strip()
    else:
        name = ""
    stats = re.findall(r"-?[\d]+",s)
    stats = [stats[x] if len(stats)-1>=x else '0' for x in range(6)]
    stats = list(map(lambda x:["=" + x],stats))
    return [name,stats]