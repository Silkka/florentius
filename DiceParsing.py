import re
import random
from NumericStringParser import NumericStringParser

def get_rolls(number_of_dice, size_of_die, number_of_dice_kept):
    rolls = []
    for j in range(number_of_dice):
        rolls.append(random.randrange(1,size_of_die+1))
    rolls.sort(reverse=True)
    roll_string = "("
    for i in range(len(rolls)):
        if number_of_dice_kept > 0:
            if i<number_of_dice_kept:
                roll_string = roll_string + str(rolls[i]) + "+"
            elif i>=number_of_dice_kept:
                roll_string = roll_string +"~~"+ str(rolls[i]) +"~~" + "+"
        elif number_of_dice_kept < 0:
            if i>=len(rolls)+number_of_dice_kept:
                roll_string = roll_string + str(rolls[i]) + "+"
            elif i<len(rolls)+number_of_dice_kept:
                roll_string = roll_string +"~~"+ str(rolls[i]) +"~~" + "+"
        else:
            roll_string = roll_string + str(rolls[i]) + "+"
    roll_string = roll_string[:-1] + ")"

    if number_of_dice_kept > 0:
        rolls = rolls[:number_of_dice_kept]
    elif number_of_dice_kept < 0:
        rolls = rolls[number_of_dice+number_of_dice_kept:]
    
    return [rolls,roll_string]

def rolls_to_string(rolls):
    s = "("
    for j in rolls:
        s = s + str(j) + " + "
    s = s[:-3] + ")"
    return s

def parse_dice(dice_re):
    first_split = dice_re.group().split('d',maxsplit=1)

    if first_split[0] == '':
        number_of_dice = 1
    else:
        number_of_dice = first_split[0]
    
    second_split = first_split[1].split('k',maxsplit=1)
    
    size_of_die = second_split[0]
    number_of_dice_kept = number_of_dice
    if len(second_split) > 1:
        number_of_dice_kept = second_split[1]

    return [int(number_of_dice),int(size_of_die),int(number_of_dice_kept)]

def prepare_dice_expression(s):
    a = re.finditer(r"[0-9]*d[0-9]+(k-?[0-9]*)?",s)
    calculatable = s 
    display = s 
    for i in a:
        [number_of_dice,size_of_die,number_of_dice_kept] = parse_dice(i)
        [rolls,roll_string] = get_rolls(number_of_dice, size_of_die, number_of_dice_kept)
        calc = str(sum(rolls))
        calculatable = calculatable.replace(i.group(),calc,1)
        display = display.replace(i.group(),roll_string,1)
    return [calculatable,display]

def prepare_scalar(s,verbose=True):
    pass

def prepare_repeats(s,nsp):
    a = re.finditer(r"repeat\([\w \+\-\*\/\^]+,[\d ]+\)",s)
    #expr = a.group()
    calculation_string = s
    display_string = s
    for i in a:
        expr = i.group()
        expr = expr[len("repeat("):-1]
        [roll, times] = expr.split(',')
        r1 = "["
        r2 = "["
        for j in range(int(times)):
            [calculatable,display] =  prepare_dice_expression(roll)
            calc = str(int(nsp.eval(calculatable)))
            r1 = r1 +  calc  + ","
            r2 = r2 + display + ","
        r1 = r1[:-1] + "]"
        r2 = r2[:-1] + "]"
        calculation_string = calculation_string.replace(i.group(), r1, 1)
        display_string = display_string.replace(i.group(), r2, 1)
    return [calculation_string,display_string]

def prepare_scalars(calc_display_list):
    [calculation_string,display_string] = calc_display_list
    a = re.finditer(r"[0-9]*d[0-9]+(k-?[0-9]*)?",calculation_string)
    for i in a:
        [number_of_dice,size_of_die,number_of_dice_kept] = parse_dice(i)
        [rolls,roll_string] = get_rolls(number_of_dice, size_of_die, number_of_dice_kept)
        calc = str(sum(rolls))
        calculation_string = calculation_string.replace(i.group(),calc,1)
        display_string = display_string.replace(i.group(),roll_string,1)    

    return [calculation_string,display_string]

def insert_scalars_to_repeats(calculation_string,nsp):
    a = re.finditer(r"\-?\d* *[\+\-\*\/\^]? *\[[\d\,\+\-\*\/\^\(\) ]*\]",calculation_string)
    result = []
    for i in a:
        expr = i.group()
        scalar_iter = re.search(r"^\-?\d+ *[\+\-\*\/\^]",expr)
        scalar = ""
        operation = ""
        if scalar_iter:
            operation = scalar_iter.group()[-1]
            scalar = scalar_iter.group()[:-1].strip()
        vector_iter = re.search(r"\[[\d\,\+\-\*\/\^\(\) ]*\]", expr)
        vector_expr = vector_iter.group()[1:-1]
        vector = vector_expr.split(',')
        for j in range(len(vector)):
            vector[j] = int(nsp.eval(scalar + operation + "("+ vector[j] + ")"))
        result.append(vector)
    return result
            
def vectors_to_string(vectors):
    s = "["
    for vector in vectors:
        for i in vector:
            s = s + str(i) +  ","
            #print(s)
        s = s[:-1] + "] + ["
    s = s[:-len(" + [")]
    return s

        
#"4d5k2 + [2d6k1,3d6]+ 5d12 + repeat(4d6k3,3)"
def test(s):
    nsp = NumericStringParser()
    #asd = "[-5d6k3, d6+9]-5d12k-2 + repeat(4d6k3 + 9, 3) + 5d12k-2 + repeat(4d6k3 + 9, 3)"
    asd = s
    asd = prepare_repeats(asd,nsp)
    asd = prepare_scalars(asd)
    #print(asd)
    asd = insert_scalars_to_repeats(asd[0],nsp)
    #print(asd)
    asd = vectors_to_string(asd)
    print(asd)

#test("25 - [2d6k1+9,2d6k1+9]")



    #if expr != None:
    #    expr = expr[len("repeat("):-1]
    #    [roll, times] = expr.split(',')
    #    for i in range(int(times)):
    #        roll_calc = "("
    #        roll_str = "("
    #    return 1
#
    #else:
    #    return None


#print(prepare_dice_expression('4d6k3+7+4d3k-2'))