import discord
import asyncio
import re
import random
from DiceParsing import *
from NumericStringParser import NumericStringParser
from sheets import Sheet_Operator
from drive import Drive_Operator
from CharacterParsing import *

SHEET_SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
SHEET_SECRET_FILE = 'client_secret_sheets.json'
SHEET_APPLICATION_NAME = 'Google Sheets API Python Quickstart'

DRIVE_SCOPES = 'https://www.googleapis.com/auth/drive'
DRIVE_SECRET_FILE = 'client_secret_drive.json'
DRIVE_APPLICATION_NAME = 'Drive API Python Quickstart'

SPREADSHEET_PREFIX = 'https://docs.google.com/spreadsheets/d/'

token = ''
with open("token") as fh:
	token = fh.read().strip('\n')
clean_id = ''
with open("cleansheet") as fh:
	clean_id = fh.read().strip('\n')

#Commands
C_ROLL = '!r '
C_CLEAN = '!character '

nsp = NumericStringParser()
sheet_operator = Sheet_Operator(SHEET_SCOPES,SHEET_SECRET_FILE,SHEET_APPLICATION_NAME)
drive_operator = Drive_Operator(DRIVE_SCOPES,DRIVE_SECRET_FILE,DRIVE_APPLICATION_NAME)
client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

    



@client.event
async def on_message(message):
    if message.content.startswith(C_ROLL):
        msg = message.content[len(C_ROLL):]
        try:
            temp = prepare_repeats(msg, nsp)
            [calculation_str, display] = prepare_scalars(temp)
            calculation = insert_scalars_to_repeats(calculation_str,nsp)
            calculation = vectors_to_string(calculation)
            if len(calculation)>0:
                tail = "\n = " + calculation
            else:
                value = str(int(nsp.eval(calculation_str)))
                if value.strip() == calculation_str.strip():
                    tail = ""
                else:
                    tail = "\n = " + value
                

            await client.send_message(message.channel,message.author.mention +": " + display + "\n = " + calculation_str + tail)
        except Exception as inst:
            print(inst)
            await client.send_message(message.channel,message.author.mention +": " + "Wrongerino")
    elif message.content.startswith(C_CLEAN):
        msg = message.content[len(C_CLEAN):].strip()
        try:
            [name,stats] = parse_character(msg)
            new_id = sheet_operator.create_copy(clean_id,name)
            sheet_operator.add_character_values(new_id,name,stats)
            drive_operator.allow_editable_anyone(new_id)
            await client.send_message(message.channel,message.author.mention +": " + SPREADSHEET_PREFIX + new_id)
        except Exception as inst:
            print(inst)
            await client.send_message(message.channel,message.author.mention +": " + "Wrongerino")

    




client.run(token)