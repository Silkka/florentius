from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import argparse

SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = 'client_secret_drive.json'
APPLICATION_NAME = 'Drive API Python Quickstart'

class Drive_Operator:

    def __init__(self,scopes,clien_secret_file,application_name):
        self.cred = None
        self.service = None
        self.get_credentials(scopes,clien_secret_file,application_name)
        http = self.cred.authorize(httplib2.Http())
        self.service = discovery.build('drive', 'v3', http=http)


    def get_credentials(self,scopes,clien_secret_file,application_name):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'drive-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(clien_secret_file, scopes)
            flow.user_agent = application_name
            flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        self.cred = credentials
    
    def allow_editable_anyone(self,file_id):
        def callback(request_id, response, exception):
            if exception:
                # Handle error
                print(exception)
            else:
                print("Permission Id: %s" % response.get('id'))
        
        batch = self.service.new_batch_http_request(callback=callback)
        user_permission = {
            'type': 'anyone',
            'role': 'writer',
        }
        batch.add(self.service.permissions().create(
                fileId=file_id,
                body=user_permission,
                fields='id',
        ))
        batch.execute()

#tykki_id = '16dqEKjicjHfL5y-syUaGor-EFYPOAlQ6GEHjtG6jrwU'
#drive_operator = Drive_Operator(SCOPES,CLIENT_SECRET_FILE,APPLICATION_NAME)
#drive_operator.allow_editable_anyone(tykki_id)


