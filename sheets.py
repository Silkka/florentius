from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import argparse



SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret_sheets.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'

class Sheet_Operator:

    def __init__(self,scopes,clien_secret_file,application_name):
        self.cred = None
        self.service = None
        self.get_credentials(scopes,clien_secret_file,application_name)
        http = self.cred.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?version=v4')
        self.service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)

        
    def get_credentials(self,scopes,clien_secret_file,application_name):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(clien_secret_file, scopes)
            flow.user_agent = application_name
            flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        self.cred = credentials

    def print_stats(self,spreadsheet_id):
        """Prints stats ('Str', 'Dex', 'Con', 'Int', 'Wis', 'Cha') from the spreadsheet with the id spreadsheet_id.
        """
        range_name = ['Str', 'Dex', 'Con', 'Int', 'Wis', 'Cha']
        result = self.service.spreadsheets().values().batchGet( spreadsheetId=spreadsheet_id, ranges=range_name).execute()
        value_ranges = result.get('valueRanges')
        for i in range(len(value_ranges)):
            print(range_name[i]+ ": " + value_ranges[i]['values'][0][0])

    def get_named_sheet_ids(self,spreadsheetId,sheet_names):
        """Returns sheet ids of the sheets with the names sheet_names from the spreadsheet with id spreadsheet_id.
        """
        sheet_metadata = self.service.spreadsheets().get(spreadsheetId=spreadsheetId).execute()
        sheets = sheet_metadata.get('sheets', '')
        result = []
        for sheet in sheets:
            if sheet['properties']['title'] in sheet_names:
                result.append(sheet['properties']['sheetId'])
        return result
    
    def get_sheet_ids(self,spreadsheetId):
        """Return all of the sheet ids.
        """
        sheet_metadata = self.service.spreadsheets().get(spreadsheetId=spreadsheetId).execute()
        sheets = sheet_metadata.get('sheets', '')
        result = []
        for sheet in sheets:
            result.append(sheet['properties']['sheetId'])
        return result


    def copy_named_sheets(self,from_id,to_id,sheet_names):
        """Copies sheets with the names sheet_names from the spreadsheet with from_id to the spreadsheet with the id to_id.
        """
        sheet_ids = self.get_named_sheet_ids(from_id,sheet_names)
        body =  {'destination_spreadsheet_id' : to_id}
        for i in sheet_ids:
            response = self.service.spreadsheets().sheets().copyTo(spreadsheetId=from_id, sheetId=i, body=body).execute()
    
    def copy_spreadsheet(self, from_id, to_id):
        sheet_ids = self.get_sheet_ids(from_id)
        body =  {'destination_spreadsheet_id' : to_id}
        for i in sheet_ids:
            response = self.service.spreadsheets().sheets().copyTo(spreadsheetId=from_id, sheetId=i, body=body).execute()

    def create_spreadsheet(self,name):
        """ Creates a new spreadsheet with the name name.
            Returns the id of the new spreadsheet.
        """
        spreadsheet_body = {
            'properties' : {'title': name}
        }
        request = self.service.spreadsheets().create(body=spreadsheet_body)
        response = request.execute()
        return response['spreadsheetId']

    def _batch(self,spreadsheet_id,requests):
        body = {
            'requests': requests
        }
        return self.service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id,body=body).execute()

    def create_copy(self, original_id, new_name):
        new_id = self.create_spreadsheet(new_name)
        self.copy_spreadsheet(original_id, new_id)
        requests = []
        #Delete the intial sheet
        requests.append({
            'deleteSheet': {
                "sheetId": 0
            }
        })
        #Rename the copied sheets
        new_sheet = self.service.spreadsheets().get(spreadsheetId=new_id).execute()
        sheets = new_sheet.get('sheets', '')
        titles_ids = []
        for sheet in sheets:
            if  "Copy of " in sheet['properties']['title']:
                new_title = sheet['properties']['title'].replace("Copy of ", "", 1)
                titles_ids.append([new_title,sheet['properties']['sheetId']])
        for i in titles_ids:
            requests.append({
                "updateSheetProperties": {
                    "properties":{
                        "sheetId": i[1],
                        "title" : i[0]
                    },
                    "fields" : "title"
                }
            })
        
        self._batch(new_id,requests)
        return new_id
    
    def add_character_values(self, spreadsheet_id, name, stats):
        data = [
            {
                'range': 'OriginalStats',
                'values': stats
            },
            {
                'range': 'Name',
                'values': [[name]]
            },

        ]
        body = {
            'valueInputOption': "USER_ENTERED",
            'data': data
        }
        result = self.service.spreadsheets().values().batchUpdate(
            spreadsheetId=spreadsheet_id, body=body).execute()

        
        



        


#sheet_operator = Sheet_Operator(SCOPES,CLIENT_SECRET_FILE,APPLICATION_NAME)
#cavetank_id = '1PdqM4AcY6MCQw1_AfS_J3x-LqmdyZ_JvqDwHLlBFgeA'
#new_dude_id = '1LLnxtlG8DS8gRPKm7DvDEvxvkFM8m71-tN8CqB26nHs'
#sheet_operator.print_stats(cavetank_id)
#sheet_operator.copy_sheets(cavetank_id, new_dude_id, ['Front', 'Back', 'Spell Sheet'])
#print(sheet_operator.create_spreadsheet('Uutta pukkaa2'))
#print(sheet_operator.get_sheet_ids(cavetank_id))
#sheet_operator.copy_spreadsheet(cavetank_id,new_dude_id)
#print(sheet_operator.create_copy(cavetank_id,'Kundin kandi'))